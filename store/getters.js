const getters = {
  userInfo: state => state.loginInfo.userInfo,
  loginToken:state=>state.loginInfo.loginToken,
  presentTheme:state=>state.theme.presentTheme,
  moneyMark:state=>state.basic.moneyMark,
  osDomain:state=>state.basic.osDomain,
  appId:state=>state.basic.appId,
  xTenant:state=>state.loginInfo.xTenant,
  storelevelData:state=>state.loginInfo.storelevelData,
}
export default getters