import storage from '@/utils/storage.js'

import {
    settingGeneral
} from '../../config/vmeitime-http/index.js'
const state = {
    moneyMark: storage.get('moneyMark'),
    osDomain:storage.get('osDomain'),
    appId:storage.get('appId')
}
const mutations = {
    SET_MONEYMARK(state, data) {
        if(data=='CNY'){
            data="￥"
        }
        state.moneyMark = data
		console.log(state.moneyMark + '哈哈哈')
        storage.set('moneyMark', data)
    },
    SET_OSDOMAIN(state, data) {
        state.osDomain = data
        storage.set('osDomain', data)
    },
    SET_APPID(state, data) {
        state.appId = data
        storage.set('appId', data)
    },
    NO_VCODE_MODAL(state, data) {
        console.log('data: ', data);
  
    },
}
const actions = {
    settingGeneralFn({
        commit,
        state,
        dispatch
    }) {
        return new Promise((resolve, reject) => {
            settingGeneral().then(res => {
                if (res) {
                    commit("SET_MONEYMARK", res.currency)
                    commit("SET_OSDOMAIN", res.osDomain)
                    commit("SET_APPID", res?.appId)
                }
            })
        }).catch(err => {
            reject(err)
        })
    },
}

export default {
    state,
    mutations,
    actions
}