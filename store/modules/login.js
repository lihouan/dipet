import storage from '@/utils/storage.js'
import {
	getUserInfo,
} from '../../config/vmeitime-http/login.js'
const state = {
	userInfo: null,
	loginToken: storage.get('loginToken')
}

const mutations = {
	LOGIN_USERINFO(state, data) {
		state.userInfo = data
		storage.set('loginInfo', data)
	},
	LOGIN_TOKEN(state, data) {
		state.loginToken = data
		storage.set('loginToken', data)
	}

}

const actions = {
	getUserInfo({
		commit,
		state,
	}) {
		return new Promise((resolve, reject) => {
			getUserInfo().then(res => {
				if (res) {
					storage.set('userInfo', res)
					commit('LOGIN_USERINFO', res)
					resolve()
				}
			}).catch(err => {
				
				reject(err)
			})

		})
	},
	logout({
		commit,
	},ignoreType) {
		console.log('ignoreType: ', ignoreType);
		return new Promise((resolve, reject) => {
			storage.remove("inviteData");
			commit('LOGIN_USERINFO', '')
			commit('LOGIN_TOKEN', '')
			
			resolve()
		})
	}

}

export default {
	state,
	mutations,
	actions,
}