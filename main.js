import App from './App'
import store from './store/index.js'

// #ifndef VUE3
import Vue from 'vue'
Vue.config.productionTip = false
App.mpType = 'app'
import inter from './config/vmeitime-http/interface'
import share  from './utils/share.js'

import {
	http
} from './config/carride-http/interface.js'
Vue.prototype.$http = http

// 全局挂载后使用
Vue.prototype.$upImgURL = inter.config.baseUrl + '/haodi/oss/upload'
Vue.prototype.$store = store
const app = new Vue({
  mode:'hash',
  store,
  ...App,
  share
})
Vue.mixin(share)
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif