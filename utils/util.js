import store from '../store/index.js'
import {
	geocoder
} from '@/config/vmeitime-http/common.js'
import storage from './storage.js'

export function showToast(title) {
	uni.showToast({
		title: title,
		icon: 'none',
		duration: 1500
	})
}

// 手机号校验
export function isValidPhoneNumber(phoneNumber) {
	return /^1\d{10}$/.test(phoneNumber);
}

// 根据经纬度跳转位置
export function openLocation(data) {
	// 假设你有一个经纬度对象
	const location = {
		latitude: data.lat,
		longitude: data.lon,
		scale: 18, // 缩放比例（可选）
		name: data.name, // 位置名（可选）
		address: data.address,
		success() {
			console.log('跳转成功');
		},
		fail(error) {
			console.error('跳转失败', error);
		}
	};

	// 调用 uni.openLocation 跳转位置
	uni.openLocation(location);
}


// 上传图片
export function chooseAndUploadImage() {
	let that = this
	let token = store.state.loginInfo.loginToken.access_token || store.getters.loginToken
	return new Promise(function(resolve, reject) {
		// 选择图片
		uni.chooseImage({
			count: 1, // 默认9，设置图片的数量
			sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
			sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
			success: (chooseImageRes) => {
				const tempFilePaths = chooseImageRes.tempFilePaths;
				const uploadTask = uni.uploadFile({
					url: that.$upImgURL, // 服务器上传接口地址
					filePath: tempFilePaths[0],
					name: 'file', // 必须填写，后台用来接收文件
					header: {
						"Authorization": `Bearer ${token}`,
						"Clientid": 'df1e3a0ca7b1f9b809eebcb01a9c6003'
					},
					success: (uploadFileRes) => {
						let data = JSON.parse(uploadFileRes.data)
						resolve(data.data.url)
						console.log('图片上传成功', data.data.url);
					},
					fail: (uploadFileErr) => {
						reject(uploadFileErr)
						console.error('图片上传失败', uploadFileErr);
					}
				});
				// 监听上传进度变化
				uploadTask.onProgressUpdate((res) => {
					console.log('上传进度' + res.progress + '%');
				});
			},
			fail: (chooseImageErr) => {
				console.error('选择图片失败', chooseImageErr);
			}
		});
	})
}

// 获取当前位置
export function currentLocation() {
	console.log('进来啊')
	return new Promise(function(resolve, reject) {

		// 获取经纬度
		uni.getLocation({
			isHighAccuracy: true, // 开启地图精准定位
			type: 'gcj02', // 坐标系类型
			altitude: true,
			success: (res) => {
				var latitude = res.latitude; // 维度
				var longitude = res.longitude; // 经度
				console.log('经度：' + longitude + '，纬度：' + latitude);
				// that.form.LATITUDE = latitude;
				// that.form.LONGITUDE = longitude;
				//加偏移
				let x = longitude
				let y = latitude
				let x_pi = (3.14159265358979324 * 3000.0) / 180.0
				let z = Math.sqrt(x * x + y * y) + 0.00002 * Math.sin(y * x_pi)
				let theta = Math.atan2(y, x) + 0.000003 * Math.cos(x * x_pi)
				let lngs = z * Math.cos(theta) + 0.0065
				let lats = z * Math.sin(theta) + 0.006
				console.log('加偏移后经度：' + lngs + '，加偏移纬度：' + lats);
				//加偏移结束
				console.log('当前位置的经度：' + lngs);
				console.log('当前位置的纬度：' + lats);
				console.log('当前速度：' + res.speed + '米/秒');
				storage.set('lonLat', {
					lon: lngs,
					lat: lats
				})
				geocoder({
					lon: lngs,
					lat: lats
				}).then((res) => {
					storage.set('currentLocation', res)
					resolve(res)
				})
			},
			fail: (err) => {
				reject(err)
				console.error('获取位置失败：', err);
			}
		});
	});
}

// 防抖
export function debounce(func, delay = 1000, immediate = false) {
	//闭包
	let timer = null
	//不能用箭头函数
	return function() {
		if (timer) {
			clearTimeout(timer)
		}
		if (immediate && !timer) {
			func.apply(this, arguments)
		}
		timer = setTimeout(() => {
			func.apply(this, arguments)
		}, delay)
	}
}


// 格式化日期
export function formatTime(timestamp, type) {
	const date = new Date(timestamp); // 如果timestamp是数值，直接使用，否则需要转换
	const year = date.getFullYear();
	const month = (date.getMonth() + 1).toString().padStart(2, '0');
	const day = date.getDate().toString().padStart(2, '0');
	const hours = date.getHours().toString().padStart(2, '0');
	const minutes = date.getMinutes().toString().padStart(2, '0');
	const seconds = date.getSeconds().toString().padStart(2, '0');
	if (type == 'date') {
		return `${year}-${month}-${day}`;
	} else if (type == 'dateTime') {
		return `${year}-${month}-${day} ${hours}:${minutes}`;
	} else if (type == 'dateTimeSeconds') {
		return `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;
	} else if (type == 'dateHours') {
		return `${year}-${month}-${day}-${hours}`;
	} else if (type == 'time') {
		return `${hours}-${minutes}`;
	} else if (type == 'year-month') {
		return `${year}-${month}`;
	} else if (type == 'dateTimex') {
		return `${year}/${month}/${day} ${hours}:${minutes}`;
	}
}

export function formatter(type, value) {
	if (type === 'year') {
		return `${value}年`
	}
	if (type === 'month') {
		return `${value}月`
	}
	if (type === 'day') {
		return `${value}日`
	}
	return value
}

// 跳转
export function gopage(url) {
	uni.navigateTo({
		url: url
	})
}


// 下载并压缩网络图片
export function downloadAndCompressImage(imageUrl) {
	return new Promise((resolve, reject) => {
		const downloadTask = uni.downloadFile({
			url: imageUrl,
			success: (downloadResult) => {
				if (downloadResult.statusCode === 200) {
					uni.compressImage({
						src: downloadResult.tempFilePath,
						quality: 80, // 压缩质量
						success: (compressResult) => {
							resolve(compressResult);
						},
						fail: (error) => {
							reject(error);
						}
					});
				} else {
					reject(new Error('下载图片失败，状态码：' + downloadResult.statusCode));
				}
			},
			fail: (error) => {
				reject(error);
			}
		});

		downloadTask.onProgressUpdate((res) => {
			console.log('下载进度' + res.progress + '%');
		});
	});
}

// 拨打电话
export function callPhone(phoneNumber) {
	uni.makePhoneCall({
		phoneNumber: phoneNumber, // 电话号码
		success: function() {
			console.log('拨号成功');
		},
		fail: function(err) {
			console.log('拨号失败：', err);
		}
	});
}


export function onLongPress(e) {
	console.log(e)
	// 在这里可以执行复制操作
	uni.setClipboardData({
		data: e,
		success: () => {
			console.log('复制成功');
			// 可以弹窗提示复制成功
			uni.showToast({
				title: '复制成功',
				icon: 'none'
			});
		},
		fail: () => {
			console.log('复制失败');
		}
	});
}


// 预览图片
export function previewImage(index, imgList) {
	uni.previewImage({
		current: index,
		urls: imgList
	});
}


// 点击获取位置
export function getLocation() {
	return new Promise(function(resolve, reject) {
		let address = {
			address: '',
			province: '',
			city: '',
			county: '',
			longitude: '',
			lat: ''
		}
		uni.chooseLocation({
			success: res => {
				console.log('res', res);
				// console.log('位置名称：' + res.name);
				// console.log('详细地址：' + res.address);
				// console.log('纬度：' + res.latitude);
				// console.log('经度：' + res.longitude);
				let locationObj = captureLocation(res);
				address.province = locationObj[0];
				address.city = locationObj[1];
				address.county = locationObj[2];
				address.address = res.address;
				address.lon = res.longitude;
				address.lat = res.latitude;
				console.log('返回地址')
				console.log(address);

				resolve(address)
			},
		})
	});
};

/**
   * 格式化位置
   * @param {*} res chooseLocation成功后返回参数
   * 格式: {
		 address: "山东省济南市槐荫区经十西路29851号"
	 errMsg: "chooseLocation:ok"
	 latitude: 36.65142
	 longitude: 116.90084
	 name: "济南市槐荫区人民政府"
	 }
   */
const captureLocation = (res) => {
	// console.log('res', res);
	var reg = /.+?(省|市|自治区|自治州|县|区)/g; // 省市区的正则
	const match = res.address.match(reg)
	console.log(match)
	return match;
}

export const groupStatusMap = {
	'0': '待支付',
	'1': '已支付',
	'2': '退款中',
	'3': '退款完成',
	'4': '交易已取消',
	'5': '交易已取消',
	'6': '售后中',
	'7': '退款中',
	'8': '退款完成',
	'9': '交易完成'
}

export const feedStatusMap = {
	0: '待支付',
	1: '待接单',
	2: '取消退款中',
	3: '已取消',
	4: '已取消',
	5: '已取消',
	6: '已接单',
	7: '服务中',
	8: '已完成',
	9: '评价完成'
}


export const licenseTypeList = [{
	name: 'A1'
}, {
	name: 'A2'
}, {
	name: 'A3'
}, {
	name: 'B1'
}, {
	name: 'B2'
}, {
	name: 'C1'
}, {
	name: 'C2'
}, {
	name: 'C3'
}, {
	name: 'C4'
}, {
	name: 'C5'
}, {
	name: 'C6'
}, {
	name: 'D'
}, {
	name: 'E'
}, {
	name: 'F'
}, {
	name: 'M'
}, {
	name: 'N'
}, {
	name: 'P'
}]