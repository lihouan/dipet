
// 身份证
export function isValidIDCard(idCard) {
  var reg = /^[1-9]\d{5}(19|20)\d{2}((0[1-9])|(1[0-2]))(([0-2][1-9])|10|20|30|31)\d{3}[\dX]$/;
  return reg.test(idCard);
}

// 手机号
export function isValidPhoneNumber(phoneNumber) {
    // 简单的中国大陆手机号码正则表达式
    var pattern = /^1[3-9]\d{9}$/;
    return pattern.test(phoneNumber);
}