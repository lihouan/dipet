import http from './interface'

//  团购商品 模块

// 评价商品
export const evaluateProducts = (data) => {
	return http.request({
		url: '/haodi/groupBuy/evaluateProducts',
		method: 'post',
		data,
	})
}

// 举报商户
export const merchantReport = (data) => {
	return http.request({
		url: '/haodi/merchant/report',
		method: 'post',
		data,
	})
}

// 添加团购商品
export const addGroupBuyGoods = (data) => {
	return http.request({
		url: '/haodi/groupBuy/addGroupBuyGoods',
		method: 'post',
		data,
	})
}

//团购商品列表
export const groupBuyGoodsPage = (params) => {
	return http.request({
		url: '/haodi/groupBuy/groupBuyGoodsPage',
		method: 'get',
		params,
	})
}

//团购商品详情
export const groupBuyGoodsInfo = (params) => {
	return http.request({
		url: '/haodi/groupBuy/groupBuyGoodsInfo',
		method: 'get',
		params,
	})
}

//团购商品评价分页列表
export const evaluatePage = (params) => {
	return http.request({
		url: '/haodi/groupBuy/evaluatePage',
		method: 'get',
		params,
	})
}

//商户评价分页列表
export const merchantEvaluatePage = (params) => {
	return http.request({
		url: '/haodi/groupBuy/merchantEvaluatePage',
		method: 'get',
		params,
	})
}

// 商品评论下的评论
export const groupBuyEvaluateChildren = (params) => {
	return http.request({
		url: '/haodi/groupBuy/groupBuyEvaluateChildren',
		method: 'get',
		params,
	})
}

// 商品评论统计
export const groupBuyEvaluateStatistics = (params) => {
	return http.request({
		url: '/haodi/groupBuy/groupBuyEvaluateStatistics',
		method: 'get',
		params,
	})
}

// 支付团购订单
export const payOrder = (data) => {
	return http.request({
		url: '/haodi/groupBuy/order/payOrder',
		method: 'post',
		data,
	})
}

// 支付
export const pay = (data) => {
	return http.request({
		url: '/haodi/groupBuy/order/payOrder',
		method: 'post',
		data,
	})
}


// 商品评论点赞
export const groupBueEvaluateLike = (data) => {
	return http.request({
		url: '/haodi/groupBuy/groupBuyEvaluateLike',
		method: 'post',
		data,
	})
}

// 商品评论点赞取消
export const groupBueEvaluateLikeCancel = (data) => {
	return http.request({
		url: '/haodi/groupBuy/groupBuyEvaluateLikeCancel',
		method: 'post',
		data,
	})
}

// 收藏团购商品
export const collectGroupBuyGoods = (data) => {
	return http.request({
		url: '/haodi/groupBuy/collectGroupBuyGoods',
		method: 'post',
		data,
	})
}

// 取消收藏团购商品
export const cancelGroupBuyGoods = (data) => {
	return http.request({
		url: '/haodi/groupBuy/cancelCollectGroupBuyGoods',
		method: 'post',
		data,
	})
}

// 创建团购订单
export const createOrder = (data) => {
	return http.request({
		url: '/haodi/groupBuy/order/createOrder',
		method: 'post',
		data,
	})
}

// 取消团购订单
export const cancelOrder = (data) => {
	return http.request({
		url: '/haodi/groupBuy/order/cancelOrder',
		method: 'post',
		data,
	})
}

// 再来一单
export const anotherOrder = (data) => {
	return http.request({
		url: '/haodi/groupBuy/order/anotherOrder',
		method: 'post',
		data,
	})
}

// 普通用户团购订单列表
export const orderPage = (params) => {
	return http.request({
		url: '/haodi/groupBuy/order/orderPage',
		method: 'get',
		params,
	})
}

// 普通用户订单详情
export const orderDetail = (params) => {
	return http.request({
		url: '/haodi/groupBuy/order/orderInfo',
		method: 'get',
		params,
	})
}


// 商户团购订单列表
export const merchantOrderPage = (params) => {
	return http.request({
		url: '/haodi/groupBuy/order/merchantOrderPage',
		method: 'get',
		params,
	})
}


// 商户订单详情
export const merchantOrderInfo = (params) => {
	return http.request({
		url: '/haodi/groupBuy/order/merchantOrderInfo',
		method: 'get',
		params,
	})
}

// 商户核销用户订单
export const writeOffOrders = (data) => {
	return http.request({
		url: '/haodi/groupBuy/order/useOrder',
		method: 'post',
		data,
	})
}

// 修改团购商品
export const editGroupBuyGoods = (data) => {
	return http.request({
		url: '/haodi/groupBuy/editGroupBuyGoods',
		method: 'post',
		data,
	})
}

// 删除团购商品
export const delGroupBuyGoods = (data) => {
	return http.request({
		url: '/haodi/groupBuy/delGroupBuyGoods',
		method: 'post',
		data,
	})
}

// 当前商户商品上架下架统计
export const shelfProductStatistics = (params) => {
	return http.request({
		url: '/haodi/groupBuy/shelfProductStatistics',
		method: 'get',
		params,
	})
}

// 获取商户经营类目
export const merchantCategory = (params) => {
	return http.request({
		url: '/haodi/groupBuy/order/merchantCategory',
		method: 'get',
		params,
	})
}



