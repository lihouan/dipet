import http from './interface'

// 发布上门喂养
export const releaseHomeFeed = (data) => {
	return http.request({
		url: '/haodi/homeFeed/releaseHomeFeed',
		method: 'post',
		data,
	})
}

// 支付上门喂养
export const payHomeFeed = (data) => {
	return http.request({
		url: '/haodi/homeFeed/payHomeFeed',
		method: 'post',
		data,
	})
}

// 评价服务
export const evaluateHomeFeed = (data) => {
	return http.request({
		url: '/haodi/homeFeed/evaluateHomeFeed',
		method: 'post',
		data,
	})
}

// 确认接单
export const confirmHomeFeed = (data) => {
	return http.request({
		url: '/haodi/homeFeed/confirmHomeFeed',
		method: 'post',
		data,
	})
}

// 完成服务 服务过程图片上传
export const completeHomeFeed = (data) => {
	return http.request({
		url: '/haodi/homeFeed/completeHomeFeed',
		method: 'post',
		data,
	})
}

// 用户取消上门喂养
export const cancelHomeFeed = (data) => {
	return http.request({
		url: '/haodi/homeFeed/cancelHomeFeed',
		method: 'post',
		data,
	})
}

// 用户取消上门喂养
export const driverCancelHomeFeed = (data) => {
	return http.request({
		url: '/haodi/homeFeed/driverCancelHomeFeed',
		method: 'post',
		data,
	})
}


// 开始服务
export const beginHomeFeed = (data) => {
	return http.request({
		url: '/haodi/homeFeed/beginHomeFeed',
		method: 'post',
		data,
	})
}

//上门喂养列表-用户查看
export const homeFeedPage = (params) => {
	return http.request({
		url: '/haodi/homeFeed/homeFeedPage',
		method: 'get',
		params,
	})
}

//上门喂养详情-用户查看	
export const homeFeedInfo = (params) => {
	return http.request({
		url: '/haodi/homeFeed/homeFeedInfo',
		method: 'get',
		params,
	})
}

//上门喂养列表-认证司机查看
export const driverHomeFeedPage = (params) => {
	return http.request({
		url: '/haodi/homeFeed/driverHomeFeedPage',
		method: 'get',
		params,
	})
}

//上门喂养详情-认证司机查看
export const driverHomeFeedInfo = (params) => {
	return http.request({
		url: '/haodi/homeFeed/driverHomeFeedInfo',
		method: 'get',
		params,
	})
}

// 获取服务价格信息
export const getHomeFeedPrice = (params) => {
	return http.request({
		url: '/haodi/homeFeed/getHomeFeedPrice',
		method: 'get',
		params,
	})
}

// 宠物打车司机订单列表
export const queryOrdersByReceiveId = (params) => {
	return http.request({
		url: '/haodi/bdOrder/queryOrdersByReceiveId',
		method: 'get',
		params,
	})
}

// 司机获取周边订单
export const queryNearOrder = (params) => {
	return http.request({
		url: '/haodi/bdOrder/queryNearOrder',
		method: 'get',
		params,
	})
}

// 宠物打用户订单列表
export const queryOrdersByMailId = (params) => {
	return http.request({
		url: '/haodi/bdOrder/queryOrdersByMailId',
		method: 'get',
		params,
	})
}

// 宠物打用户取消订单
export const userCancelOrder = (data) => {
	return http.request({
		url: '/haodi/bdOrder/cancelOrder',
		method: 'post',
		data
	})
}

// 司机打车取消接单
export const driverCancelOrder = (data) => {
	return http.request({
		url: '/haodi/bdOrder/driverCancelOrder',
		method: 'post',
		data
	})
}

// 司机接单
export const driverRobOrder = (data) => {
	return http.request({
		url: '/haodi/bdOrder/robOrder',
		method: 'post',
		data
	})
}

// 司机签到
export const driverSign = (data) => {
	return http.request({
		url: '/haodi/bdOrder/driverSign',
		method: 'post',
		data
	})
}

// 司机完成订单
export const driverFinishOrder = (data) => {
	return http.request({
		url: '/haodi/bdOrder/driverFinishOrder',
		method: 'post',
		data
	})
}

// 司机获取订单详情
export const queryDriverOrderDetail = (params) => {
	return http.request({
		url: '/haodi/bdOrder/queryDriverOrderDetail',
		method: 'get',
		params
	})
}

// 用户确认上车
export const passengerConfirm = (data) => {
	return http.request({
		url: '/haodi/bdOrder/passengerConfirm',
		method: 'post',
		data
	})
}

// 乘客确认完成
export const passengerConfirmation = (data) => {
	return http.request({
		url: '/haodi/bdOrder/passengerConfirmation',
		method: 'post',
		data
	})
}