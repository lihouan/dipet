/**
 * 通用uni-app网络请求
 * 基于 Promise 对象实现更简单的 request 使用方式，支持请求和响应拦截
 */

import store from '../../store/index'
import {
	i18n
} from '../../main'

let that = this
export default {
	config: {
		//baseUrl: "https://sd.zecec.cn/api", //线上
		baseUrl: "http://chinamind.vicp.cc:28080", //线下
		//baseUrl: "http://120.33.92.39:28080", //线下
		
		header: {
			'Content-Type': 'application/json;charset=UTF-8',
			//'Content-Type':'application/x-www-form-urlencoded'
			// 'appTken':uni.getStorageSync('appToken')
		},
		data: {},
		method: "GET",
		dataType: "json",
		/* 如设为json，会对返回的数据做一次 JSON.parse */
		responseType: "text",
		success() {},
		fail() {},
		complete() {}
	},
	getbaseUrl() {
		return this.config.baseUrl
	},

	interceptor: {
		request: (config) => {
			// 初始化请求拦截器时，会执行此方法，此时data为undefined，赋予默认{}
			config.data = config.data || {}
			let token = store.state.loginInfo.loginToken.access_token || store.getters.loginToken
			
			// if(!token || !storage.get('userInfo')) {
			// 	uni.reLaunch({
			// 		url: "/pagesA/login/login",
			// 	});
			// 	return false
			// }
			// 根据custom参数中配置的是否需要token，添加对应的请求头
			if (!config?.custom?.auth) {
				// 可以在此通过vm引用vuex中的变量，具体值在vm.$store.state中
				config.header["Authorization"] = `Bearer ${token}`
				config.header["Clientid"] = 'df1e3a0ca7b1f9b809eebcb01a9c6003'
				
			}
		},
		response: null
	},
	request(options) {
		if (!options) {
			options = {}
		}
		options.baseUrl = options.baseUrl || this.config.baseUrl
		options.dataType = options.dataType || this.config.dataType
		options.url = options.baseUrl + options.url
		if (options.params) {
			let jsonData = options.params || options.params.params
			// 将JSON对象转换成查询字符串
			var queryString = Object.keys(jsonData).map(function(key) {
				return encodeURIComponent(key) + '=' + encodeURIComponent(jsonData[key]);
			}).join('&');

			options.url = options.url + '?' + queryString
		} else if (options.data) {
			options.data = options.data || {}
		}


		options.method = options.method || this.config.method
		return new Promise((resolve, reject) => {
			let _config = null
			options.complete = (response) => {
				let statusCode = response.statusCode
				response.config = _config
				if (process.env.NODE_ENV === 'development') {
					if (statusCode === 200) {
						// console.log("【" + _config.requestId + "】 结果：" + JSON.stringify(response.data))
					}
				}
				if (this.interceptor.response) {
					let newResponse = this.interceptor.response(response)
					if (newResponse) {
						response = newResponse
					}
				}
				// 统一的响应日志记录
				_reslog(response)
				if ((statusCode === 200 && response.data.code==200)  || statusCode === 204) { //成功
					resolve(response.data.data);
					return false
				} else {
					// 对响应错误做点什么 （statusCode !== 200）
					console.log("请求错误！", response.data.msg)
					uni.hideLoading();
					let errInfo = response.data.msg
					if (response.data?.code == 504) { //网络异常
						errInfo = '网关超时,请稍后再试'
					}else if (response.data?.code == 500) {
						errInfo = response.data?.msg
					} else if (response.data?.code == 401) { //重新登录
						errInfo = response.data?.msg
						if (response.data.path !== '/front/auth/token') {
							store.dispatch("logout").then(() => {
								setTimeout(() => {
									uni.reLaunch({
										url: "/pagesA/login/login",
									});
								}, 1000);
							});
						}
					} else {
						if (response.data?.msg) {
							errInfo = response.data.msg
						} else if (response.data?.title) {
							errInfo = response.data.title
						}
					}
					if(errInfo == '您还未申请商户!' || errInfo == '认证失败，无法访问系统资源') {
						return false
					}
					if(errInfo) {
					  uni.showToast({
					  	title: errInfo,
					  	duration: 2000,
					  	icon: 'none'
					  });	
					}
					
				}
			}
			_config = Object.assign({}, this.config, options)
			_config.requestId = new Date().getTime()
			if (this.interceptor.request) {
				this.interceptor.request(_config)
			}
			// 统一的请求日志记录
			_reqlog(_config)
			if (process.env.NODE_ENV === 'development') {
				// console.log("【" + _config.requestId + "】 地址：" + _config.url)
				if (_config.data) {
					// console.log("【" + _config.requestId + "】 参数：" + JSON.stringify(_config.data))
				}
			}

			uni.request(_config);
		});
	},
	get(url, data, options) {
		if (!options) {
			options = {}
		}
		options.url = url
		options.data = data
		options.method = 'GET'
		return this.request(options)
	},
	post(url, data, options) {
		if (!options) {
			options = {}
		}
		options.url = url
		options.data = data
		options.method = 'POST'
		return this.request(options)
	},
	put(url, data, options) {
		if (!options) {
			options = {}
		}
		options.url = url
		options.data = data
		options.method = 'PUT'
		return this.request(options)
	},
	delete(url, data, options) {
		if (!options) {
			options = {}
		}
		options.url = url
		options.data = data
		options.method = 'DELETE'
		return this.request(options)
	}
}


/**
 * 请求接口日志记录
 */
function _reqlog(req) {
	if (process.env.NODE_ENV === 'development') {
		// console.log("【" + req.requestId + "】 地址：" + req.url)
		if (req.data) {
			// console.log("【" + req.requestId + "】 请求参数：" + JSON.stringify(req.data))
		}
	}
	//TODO 调接口异步写入日志数据库
}

/**
 * 响应接口日志记录
 */
function _reslog(res) {
	let _statusCode = res.statusCode;
	if (process.env.NODE_ENV === 'development') {
		// console.log("【" + res.config.requestId + "】 地址：" + res.config.url)
		if (res.config.data) {
			// console.log("【" + res.config.requestId + "】 请求参数：" + JSON.stringify(res.config.data))
		}
		// console.log("【" + res.config.requestId + "】 响应结果：" + JSON.stringify(res))
	}
	//TODO 除了接口服务错误外，其他日志调接口异步写入日志数据库
	switch (_statusCode) {
		case 200:
			break;
		case 401:
			break;
		case 404:
			break;
		default:
			break;
	}
}