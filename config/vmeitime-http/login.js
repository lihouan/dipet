import http from './interface'

// 登录、用户信息 模块

// 获取短信验证码
export const getVerificationCode = (data) => {
	return http.request({
		url: '/haodi/login/verificationCode',
		method: 'post',
		data,
	})
}

// 登陆
export const hdLogin = (data) => {
	return http.request({
		url: '/haodi/login/hdLogin',
		method: 'post',
		data,
	})
}

// 退出登录
export const logout = (data) => {
	return http.request({
		url: '/haodi/user/logout',
		method: 'post',
		data,
	})
}

// 关注用户
export const follow = (data) => {
	return http.request({
		url: '/haodi/user/follow',
		method: 'post',
		data,
	})
}

//获取用户信息
export const getUserInfoById = (params) => {
	return http.request({
		url: '/haodi/user/userInfo',
		method: 'get',
		params,
	})
}

//获取关注用户
export const userFollow = (params) => {
	return http.request({
		url: '/haodi/user/userFollow',
		method: 'get',
		params,
	})
}

//获取用户信息
export const getUserInfo = () => {
	return http.request({
		url: '/haodi/user/info',
		method: 'get'
	})
}

// 修改用户信息
export const editUserInfo = (data) => {
	return http.request({
		url: '/haodi/user/editUserInfo',
		method: 'post',
		data,
	})
}

// 获取openid
export const getOpenId = (data) => {
	return http.request({
		url: '/haodi/wx/login/miniAppLogin',
		method: 'post',
		data,
	})
}


//2.7-我的-笔记统计 我赞过     点过赞的笔记收藏    点收藏的笔记浏览记录    看过的笔记 评论中心    评论过的笔记
export const userPetNodeCount = () => {
	return http.request({
		url: '/haodi/user/userPetNodeCount',
		method: 'get'
	})
}

// 2.8-我的-笔记统计获赞  自己笔记获赞数量 收藏  自己笔记被收藏数量 关注  自己被关注数量
export const userPetNodeCount2 = () => {
	return http.request({
		url: '/haodi/user/userPetNodeCount2',
		method: 'get'
	})
}

// 消息通知关注
export const messageNoticeFollow = () => {
	return http.request({
		url: '/haodi/messageNotice/follow',
		method: 'get'
	})
}

// 系统消息
export const systemNotice = () => {
	return http.request({
		url: '/haodi/messageNotice/systemNotice',
		method: 'get'
	})
}

// 消息通知赞和收藏
export const likeAndCollect = () => {
	return http.request({
		url: '/haodi/messageNotice/likeAndCollect',
		method: 'get'
	})
}

// 系统消息通知详情
export const messageNoticeDetail = () => {
	return http.request({
		url: '/haodi/messageNotice/detail',
		method: 'get'
	})
}

// 消息通知评论
export const commentAndAt = () => {
	return http.request({
		url: '/haodi/messageNotice/commentAndAt',
		method: 'get'
	})
}

// 删除关注通知
export const noticeDel = (data) => {
	return http.request({
		url: '/haodi/messageNotice/delete',
		method: 'post',
		data,
	})
}

// 未读消息统计
export const unreadCount = () => {
	return http.request({
		url: '/haodi/messageNotice/unreadCount',
		method: 'get'
	})
}

// 15.1.2-通过类型更新为已读
 // "0", "系统通知"
 // "1", "用户关注"
 // "2", "笔记评论"
 // "3", "笔记点赞"
 // "4", "笔记收藏"
 // "5", "笔记@"
 // 传入消息类型,将所有未读设置成已读
export const updateRead = (data) => {
	return http.request({
		url: '/haodi/messageNotice/updateRead',
		method: 'post',
		data
	})
}