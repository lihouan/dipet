import http from './interface'

//  寻主业务 模块

// 举报寻宠信息
export const findMasterReport = (data) => {
	return http.request({
		url: '/haodi/findMaster/report',
		method: 'post',
		data,
	})
}

// 发布寻主信息
export const findMasterRelease = (data) => {
	return http.request({
		url: '/haodi/findMaster/release',
		method: 'post',
		data,
	})
}

// 已找到主人
export const findMaster = (data) => {
	return http.request({
		url: '/haodi/findMaster/found',
		method: 'post',
		data,
	})
}

// 寻主评论
export const findMasterEvaluate = (data) => {
	return http.request({
		url: '/haodi/findMaster/evaluate',
		method: 'post',
		data,
	})
}

// 寻主评论点赞
export const evaluateLike = (data) => {
	return http.request({
		url: '/haodi/findMaster/evaluateLike',
		method: 'post',
		data,
	})
}

// 编辑寻主信息
export const findMasterEdit = (data) => {
	return http.request({
		url: '/haodi/findMaster/edit',
		method: 'post',
		data,
	})
}

// 删除寻主信息
export const findMasterDel = (data) => {
	return http.request({
		url: '/haodi/findMaster/del',
		method: 'post',
		data
	})
}

// 收藏
export const findMasterInfoCollect = (data) => {
	return http.request({
		url: '/haodi/findMaster/findMasterInfoCollect',
		method: 'post',
		data
	})
}

// 寻主信息收藏取消
export const findMasterInfoCollectCancel = (data) => {
	return http.request({
		url: '/haodi/findMaster/findMasterInfoCollectCancel',
		method: 'post',
		data,
	})
}


// 寻主评论点赞
export const findMasterEvaluateLike = (data) => {
	return http.request({
		url: '/haodi/findMaster/evaluateLike',
		method: 'post',
		data,
	})
}

// 寻主评论点赞取消
export const findMasterEvaluateLikeCancel = (data) => {
	return http.request({
		url: '/haodi/findMaster/evaluateLikeCancel',
		method: 'post',
		data,
	})
}


//寻主信息详情
export const findMasterInfo = (params) => {
	return http.request({
		url: '/haodi/findMaster/info',
		method: 'get',
		params,
	})
}

//寻主信息分页列表
export const findMasterPage = (params) => {
	return http.request({
		url: '/haodi/findMaster/findMasterPage',
		method: 'get',
		params,
	})
}

//寻主评论分页列表
export const findMasterEvaluatePage = (params) => {
	return http.request({
		url: '/haodi/findMaster/evaluatePage',
		method: 'get',
		params,
	})
}


// 寻宠评论的评论分页列表
export const findMasterChildrenEvaluatePage = (params) => {
	return http.request({
		url: '/haodi/findMaster/childrenEvaluatePage',
		method: 'get',
		params,
	})
}
