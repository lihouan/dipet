import http from './interface'

//  商户功能 模块

// 举报商户
export const merchantReport = (data) => {
	return http.request({
		url: '/haodi/merchant/report',
		method: 'post',
		data,
	})
}

// 商户申请
export const merchantRegister = (data) => {
	return http.request({
		url: '/haodi/merchant/register',
		method: 'post',
		data,
	})
}

// 商户申请修改 只允许修改待提交和驳回的申请
export const merchantRegisterEdit = (data) => {
	return http.request({
		url: '/haodi/merchant/registerEdit',
		method: 'post',
		data,
	})
}

// 修改商品
export const merchantEditGoods = (data) => {
	return http.request({
		url: '/haodi/merchant/editGoods',
		method: 'post',
		data,
	})
}

// 删除商品
export const merchantDelGoods = (data) => {
	return http.request({
		url: '/haodi/merchant/delGoods',
		method: 'post',
		data,
	})
}

// 商户信息修改
export const editMerchantInfo = (data) => {
	return http.request({
		url: '/haodi/merchant/editMerchantInfo',
		method: 'post',
		data,
	})
}



// 收藏商家
export const merchantCollect = (data) => {
	return http.request({
		url: '/haodi/merchant/collectingMerchants',
		method: 'post',
		data,
	})
}

// 取消商家
export const cancelCollectingMerchants = (data) => {
	return http.request({
		url: '/haodi/merchant/cancelCollectingMerchants',
		method: 'post',
		data,
	})
}

// 添加商品
export const addGoods = (data) => {
	return http.request({
		url: '/haodi/merchant/addGoods',
		method: 'post',
		data,
	})
}

// 生成商家分享图片
export const sharePictures = (params) => {
	return http.request({
		url: '/haodi/merchant/sharePictures',
		method: 'get',
		params,
	})
}

// 商户申请详情
export const registerInfo = (params) => {
	return http.request({
		url: '/haodi/merchant/registerInfo',
		method: 'get',
		params,
	})
}

// 商户列表分页
export const merchantPage = (params) => {
	return http.request({
		url: '/haodi/merchant/page',
		method: 'get',
		params,
	})
}

// 商户列表分页带团购商品
export const merchantAndGoodsPage = (params) => {
	return http.request({
		url: '/haodi/merchant/merchantAndGoodsPage',
		method: 'get',
		params,
	})
}

// 商户详情
export const merchantInfo = (params) => {
	return http.request({
		url: '/haodi/merchant/info',
		method: 'get',
		params,
	})
}

// 商品分页列表
export const goodsPage = (params) => {
	return http.request({
		url: '/haodi/merchant/goodsPage',
		method: 'get',
		params,
	})
}

// 商户订单信息统计
export const merchantStatistics = (params) => {
	return http.request({
		url: '/haodi/merchant/merchantStatistics',
		method: 'get',
		params,
	})
}

// 商户管理-资金信息
export const merchantMoneyInfo = (params) => {
	return http.request({
		url: '/haodi/merchant/merchantMoneyInfo',
		method: 'get',
		params,
	})
}

// 商户管理-商品销售信息统计
export const goodsSaleStatistics = (params) => {
	return http.request({
		url: '/haodi/merchant/goodsSaleStatistics',
		method: 'get',
		params,
	})
}

// 商户管理-保证金流水
export const depositFlow = (params) => {
	return http.request({
		url: '/haodi/merchant/depositFlow',
		method: 'get',
		params,
	})
}

// 商户管理-保证金提现
export const depositWithdrawal = (data) => {
	return http.request({
		url: '/haodi/merchant/depositWithdrawal',
		method: 'post',
		data,
	})
}

// 商户管理-保证金充值
export const depositRecharge = (data) => {
	return http.request({
		url: '/haodi/merchant/depositRecharge',
		method: 'post',
		data,
	})
}

// 3-1.5-商户可提现余额提现申请
export const sqwithdrawal = (data) => {
	return http.request({
		url: '/haodi/merchant/cash/withdrawal',
		method: 'post',
		data,
	})
}

// 3-1.7-商户提现记录
export const withdrawalRecord = (params) => {
	return http.request({
		url: '/haodi/merchant/cash/withdrawalRecord',
		method: 'get',
		params,
	})
}

// 3-1.6-商户余额明细
export const withdrawalList = (params) => {
	return http.request({
		url: '/haodi/merchant/cash/withdrawalList',
		method: 'get',
		params,
	})
}

// 3-1.1-银行卡列表
export const bankList = (params) => {
	return http.request({
		url: '/haodi/merchant/cash/bankList',
		method: 'get',
		params,
	})
}

// 3-1.2-添加银行卡
export const addBank = (data) => {
	return http.request({
		url: '/haodi/merchant/cash/addBank',
		method: 'post',
		data,
	})
}

//3-1.3-修改银行卡
export const updateBank = (data) => {
	return http.request({
		url: '/haodi/merchant/cash/updateBank',
		method: 'post',
		data,
	})
}

//3-1.4-删除银行卡
export const delBank = (data) => {
	return http.request({
		url: '/haodi/merchant/cash/delBank',
		method: 'post',
		data,
	})
}

// 3-1.8-提现手续费
export const withdrawalFee = (params) => {
	return http.request({
		url: '/haodi/merchant/cash/withdrawalFee',
		method: 'get',
		params,
	})
}
