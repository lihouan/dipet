import http from './interface'

//  通用接口模块

// 上传OSS对象存储
export const upload = (data) => {
	return http.request({
		url: '/haodi/oss/upload',
		method: 'post',
		data,
	})
}

// 返回省份列表数据
export const provincialList = (params) => {
	return http.request({
		url: '/haodi/common/provincialList',
		method: 'get',
		params,
	})
}

// 根据省份ID返回城市列表
export const cityList = (params) => {
	return http.request({
		url: '/haodi/common/cityList',
		method: 'get',
		params,
	})
}

// 获取启用状态的banner
export const bannerList = (params) => {
	return http.request({
		url: '/haodi/common/bannerList',
		method: 'get',
		params,
	})
}

// 商户经营类目
export const businessCategory = (params) => {
	return http.request({
		url: '/haodi/common/businessCategory',
		method: 'get',
		params,
	})
}

// 枚举
export const getDictionary = (params) => {
	return http.request({
		url: '/haodi/common/getDictionary',
		method: 'get',
		params,
	})
}

// 天地图传入坐标返回地址信息
export const geocoder = (params) => {
	return http.request({
		url: '/haodi/common/geocoder',
		method: 'get',
		params,
	})
}