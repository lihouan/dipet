import http from './interface'

//  萌宠笔记 模块



// 评论笔记
export const noteEvaluate = (data) => {
	return http.request({
		url: '/haodi/pets/note/evaluate',
		method: 'post',
		data,
	})
}

// 评论列表
export const evaluatePage = (params) => {
	return http.request({
		url: '/haodi/pets/note/evaluatePage',
		method: 'get',
		params,
	})
}



// 修改笔记
export const noteEdit = (data) => {
	return http.request({
		url: '/haodi/pets/note/edit',
		method: 'post',
		data,
	})
}

// 删除笔记
export const noteDel = (data) => {
	return http.request({
		url: '/haodi/pets/note/del',
		method: 'post',
		data,
	})
}

// 评论点赞
export const commentLike = (data) => {
	return http.request({
		url: '/haodi/pets/note/commentLike',
		method: 'post',
		data,
	})
}

// 取消评论点赞
export const commentLikeCancel = (data) => {
	return http.request({
		url: '/haodi/pets/note/commentLikeCancel',
		method: 'post',
		data,
	})
}

// 笔记点赞
export const noteLike = (data) => {
	return http.request({
		url: '/haodi/pets/note/like',
		method: 'post',
		data,
	})
}

// 取消笔记点赞
export const noteLikeCancel = (data) => {
	return http.request({
		url: '/haodi/pets/note/likeCancel',
		method: 'post',
		data,
	})
}



// 收藏笔记
export const noteCollect = (data) => {
	return http.request({
		url: '/haodi/pets/note/collect',
		method: 'post',
		data,
	})
}

// 取消收藏笔记
export const collectCancel = (data) => {
	return http.request({
		url: '/haodi/pets/note/collectCancel',
		method: 'post',
		data,
	})
}

// 4.11-笔记评论的评论列表
export const childrenEvaluatePage = (params) => {
	return http.request({
		url: '/haodi/pets/note/childrenEvaluatePage',
		method: 'get',
		params,
	})
}


// 添加笔记
export const noteAdd = (data) => {
	return http.request({
		url: '/haodi/pets/note/add',
		method: 'post',
		data,
	})
}

//获取笔记列表
export const notePage = (params) => {
	return http.request({
		url: '/haodi/pets/note/page',
		method: 'get',
		params,
	})
}

//笔记详情
export const noteInfo = (params) => {
	return http.request({
		url: '/haodi/pets/note/info',
		method: 'get',
		params,
	})
}

// 添加笔记
export const follow = (data) => {
	return http.request({
		url: '/haodi/user/follow',
		method: 'post',
		data,
	})
}

// 取消关注
export const followCancel = (data) => {
	return http.request({
		url: '/haodi/user/followCancel',
		method: 'post',
		data,
	})
}


// 我评论过的笔记列表
export const myEvaluateList = (params) => {
	return http.request({
		url: '/haodi/pets/note/evaluateList',
		method: 'get',
		params,
	})
}

// 我收藏的笔记列表
export const myCollectList = (params) => {
	return http.request({
		url: '/haodi/pets/note/collectList',
		method: 'get',
		params,
	})
}

// 我赞过的笔记列表
export const mylikeList = (params) => {
	return http.request({
		url: '/haodi/pets/note/likeList',
		method: 'get',
		params,
	})
}

// 我浏览过的笔记列表
export const mybrowseList = (params) => {
	return http.request({
		url: '/haodi/pets/note/browseList',
		method: 'get',
		params,
	})
}