import http from './interface'

//  寻宠业务 模块

// 举报寻宠信息
export const findPetsReport = (data) => {
	return http.request({
		url: '/haodi/findPets/report',
		method: 'post',
		data,
	})
}



// 寻宠信息收藏
export const findPetsInfoCollect = (data) => {
	return http.request({
		url: '/haodi/findPets/findPetsInfoCollect',
		method: 'post',
		data,
	})
}

// 寻宠信息收藏取消
export const findPetsInfoCollectCancel = (data) => {
	return http.request({
		url: '/haodi/findPets/findPetsInfoCollectCancel',
		method: 'post',
		data,
	})
}


// 发布寻宠信息
export const findPetsRelease = (data) => {
	return http.request({
		url: '/haodi/findPets/release',
		method: 'post',
		data,
	})
}

// 已找到宠物
export const findPetsFound = (data) => {
	return http.request({
		url: '/haodi/findPets/found',
		method: 'post',
		data,
	})
}

// 寻宠评论
export const findPetsEvaluate = (data) => {
	return http.request({
		url: '/haodi/findPets/evaluate',
		method: 'post',
		data,
	})
}

// 寻宠评论点赞
export const findPetsEvaluateLike = (data) => {
	return http.request({
		url: '/haodi/findPets/evaluateLike',
		method: 'post',
		data,
	})
}

// 寻宠评论点赞取消
export const findPetsEvaluateLikeCancel = (data) => {
	return http.request({
		url: '/haodi/findPets/evaluateLikeCancel',
		method: 'post',
		data,
	})
}


// 编辑寻宠信息
export const findPetsEdit = (data) => {
	return http.request({
		url: '/haodi/findPets/edit',
		method: 'post',
		data,
	})
}

// 删除寻宠信息
export const findPetsDel = (data) => {
	return http.request({
		url: '/haodi/findPets/del',
		method: 'post',
		data,
	})
}

// 寻宠信息详情
export const findPetsInfo = (params) => {
	return http.request({
		url: '/haodi/findPets/info',
		method: 'get',
		params,
	})
}

// 寻宠信息分页列表
export const findPetsPage = (params) => {
	return http.request({
		url: '/haodi/findPets/findPetsPage',
		method: 'get',
		params,
	})
}

// 寻宠评论分页列表
export const evaluatePage = (params) => {
	return http.request({
		url: '/haodi/findPets/evaluatePage',
		method: 'get',
		params,
	})
}

// 寻宠评论的评论分页列表
export const findPetsChildrenEvaluatePage = (params) => {
	return http.request({
		url: '/haodi/findPets/childrenEvaluatePage',
		method: 'get',
		params,
	})
}




