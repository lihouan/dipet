import http from './interface'

// 获取保价金额
export const getInsurance = (params) => {
	return http.request({
		url: '/order/getInsurance',
		method: 'get',
		params,
	})
}

// 计算价格
export const calcOrderPrice = (params) => {
	return http.request({
		url: '/order/orderPrice',
		method: 'get',
		params,
	})
}
