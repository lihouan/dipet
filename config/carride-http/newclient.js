import http from '../vmeitime-http/interface'

// 创建订单
export const createOrder = (data) => {
	return http.request({
		url: '/haodi/bdOrder/createOrder',
		method: 'post',
		data
	})
}

// 司机认证
export const saveDriver = (data) => {
	return http.request({
		url: '/haodi/driver/saveDriver',
		method: 'post',
		data
	})
}

// 根据id获取司机信息
export const myDriverInfo = (params) => {
	return http.request({
		url: '/haodi/driver/myDriverInfo',
		method: 'get',
		params,
	})
}
