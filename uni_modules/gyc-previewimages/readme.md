# gyc-previewimages

### 这是一个预览图片的组件，功能类似于uni.previewImage；
### 使用该组件可避免调用uni.previewImage api时所产生触发onshow，onhide的问题；
### 近期发现uni.previewImage在微信小程序中，部分安卓机型存在图片显示黑屏的问题，使用此组件可避免该问题；

### 可参考下面使用例子：

```vue
<template>
	<view>
		<image class="cover" v-for="(item,idx) in imgsList" :key="idx" :src="item" @click="handlerImgs(idx)"></image>
		<!-- 引入组件 -->
		<gyc-previewimages ref="imgs"></gyc-previewimages>
	</view>
</template>

<script>
	export default {
		data() {
			return {
				// 图片列表，自行填入地址
				imgsList: [
					"https://iph.href.lu/500x500",
					"https://iph.href.lu/500x1800?fg=666666&bg=cccccc"
				]
			}
		},
		methods: {
			/**
			 * 预览图片
			 * @param {Object} idx 图片索引
			 */
			handlerImgs(idx) {
				// 调用组件内previewImages方法
				// 该方法传入current参数（当前展示的图片的索引），urls参数（当前展示的图片列表，格式参考imgsList）
				this.$refs.imgs.previewImages({
					current: idx,
					urls: this.imgsList
				})
			}
		}
	}
</script>

<style scoped lang="scss">
	.cover {
		width: 300rpx;
		height: 300rpx;
	}
</style>
```
